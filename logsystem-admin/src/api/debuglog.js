import axios from '@/libs/api.request';

const controller = 'api/DebugLog/';

export const query = (queryData) => {
    const data = queryData;
    return axios.request({
        url: controller + 'Query',
        params: data,
        method: 'get'
    });
};

export const remove = (ids) => {
    const data = ids;
    return axios.request({
        url: controller + 'Delete',
        data: data,
        method: 'delete'
    });
};

import Vue from 'vue';
import iView from 'iview';
import router from './router';
import store from './store';
import App from './app.vue';
import 'iview/dist/styles/iview.css';
import socketInstance from '@/libs/socket';
import {mapMutations} from 'vuex';
import axios from 'axios';

Vue.use(iView);

iView.LoadingBar.config({
    color: '#2d8cf0',
    height: 3
});

new Vue({
    el: '#app',
    router: router,
    store: store,
    render: h => h(App),
    methods: {
        ...mapMutations([
            'setTcpServiceData'
        ]),
        handleMessage(data) {
            this.setTcpServiceData(data);
        }
    },
    mounted() {

        axios.get('/static/config.json').then(response => {
            window.configData = response.data;

            let instance = socketInstance(response.data.webSocketUrl);
            instance.onopen = (msg) => {
                console.log(msg);
            };
            instance.onmessage = (msg) => {
                if (msg.data) {
                    this.handleMessage(JSON.parse(msg.data));
                }
            };
            instance.onclose = (msg) => {
                console.log('connection closed.' + msg);
            };
        }).catch(error => {
            console.log(error);
        });
    }
});

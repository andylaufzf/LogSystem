import {QueryPlatforms} from '@/api/commondata';

export default {
    namespaced: true,
    state: {
        Platforms: []
    },
    mutations: {
        setPlatforms(state, list) {
            state.Platforms = list;
        }
    },
    actions: {
        handleStoreQueryPlatforms(context) {
            return new Promise((resolve, reject) => {
                QueryPlatforms().then(res => {
                    context.commit('setPlatforms', res.Data);
                    resolve(res);
                }).catch(err => {
                    reject(err);
                });
            });
        }
    }
};

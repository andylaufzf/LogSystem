import {formatDate} from '@/libs/tools';

const dateTimeFmt = 'yyyy-MM-dd hh:mm:ss';
let tableColumns = {};

tableColumns.columns = (vue) => {
    return [
        {title: '匹配字段', key: 'Name', sortable: true},
        {title: '匹配规则', key: 'AppId', sortable: true},
        {title: '匹配值', key: 'AppSecrect', sortable: false},
    ];
};

export default tableColumns;
